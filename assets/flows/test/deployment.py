from hello import hello_world

from prefect.deployments import Deployment
from prefect.filesystems import RemoteFileSystem
from prefect.infrastructure import Process
import os

MINIO_ENDPOINT = os.getenv("MINIO_ENDPOINT")
MINIO_KEY=os.getenv("MINIO_KEY")
MINIO_SECRET=os.getenv("MINIO_SECRET")

minio_block = RemoteFileSystem(
    basepath="s3://prefect-flows/",
    key_type="hash",
    settings=dict(
        use_ssl=False,
        key=MINIO_KEY,
        secret=MINIO_SECRET,
        client_kwargs=dict(endpoint_url=MINIO_ENDPOINT)
    ),
)
minio_block.save("teste", overwrite=True)

# Hello World deployment
deployment_hello_world = Deployment.build_from_flow(
    name="World",
    flow=hello_world,
    storage=RemoteFileSystem.load('teste'),
    infrastructure=Process(
        name="Hello World flow process",
        env={"ENDPOINT_URL": MINIO_ENDPOINT}
    )
)
deployment_hello_world.apply()
