from prefect import flow, get_run_logger


@flow(name="Hello")
def hello_world():
    logger = get_run_logger()
    logger.info("Hello from Prefect Flow!")
