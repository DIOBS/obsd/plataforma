![Observatório em Dados](./assets/img/obsd.png){width=40%}

# Descrição

O Observatório em Dados é uma plataforma desenvolvida pelo [Observatório de Fortaleza](https://observatoriodefortaleza.fortaleza.ce.gov.br/) voltada para a gestão do fluxo dos dados utilizados no monitoramento de políticas públicas, desde a coleta dos dados até a sua análise e publicação.

O Observatório auxilia no processo de publicização dos dados e análises feitas pelo Instituto de Planejamento de Fortaleza, tornando o processo de tomada de decisões da Prefeitura acessíveis e compreensíveis aos gestores públicos e aos cidadãos.

# Requisitos

- [Docker](https://www.docker.com/) (com a ferramenta [compose](https://docs.docker.com/compose/) habilitada);
- [GNU Make](https://www.gnu.org/software/make/) (para a construção automatizada dos serviços).

# Construindo as imagens localmente

Esta etapa é necessária tanto no ambiente de desenvolvimento quanto no de produção. Para fazer o download das e construir as imagens do Docker necessárias, execute:

```
docker compose build
```

Com isso você pode checar as imagens com prefixo `obsd/*` com 
```
docker image ls | grep 'obsd/'
```

# Credenciais de acesso

As credenciais de acesso aos serviços, como usuários e senhas, devem ser definidas no arquivo `.env`. Veja o arquivo `compose.yaml` para ver as variáveis necessárias.

# Inicializando os serviços

```
docker compose up
```

# Acessando os serviços localmente

No ambiente de desenvolvimento, as portas de acesso aos serviços estão expostas. Assim, você pode acessar os containers através das portas de acesso:

- Prefect UI: http://localhost:4200
- MinIO: http://localhost:9000
- Shiny Server: http://localhost:3838
- Superset: http://localhost:8088
