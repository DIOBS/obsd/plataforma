import os

REDIS_SERVER = os.getenv("REDIS_SERVER")
REDIS_PORT = os.getenv("REDIS_PORT")

SECRET_KEY = str(os.getenv("SUP_SECRET_KEY"))
SQLALCHEMY_DATABASE_URI = os.getenv("SUPERSET_SQLALCHEMY_DATABASE_URI")

############
# App info #
############
# Setup App name
APP_NAME = "Observatório em Dados"
# Specify the App icon
APP_ICON = "/static/assets/images/obsd-logo-horiz.png"
# Specify tooltip that should appear when hovering over the App Icon/Logo
LOGO_TOOLTIP = APP_NAME
# Specify any text that should appear to the right of the logo
# LOGO_RIGHT_TEXT: Union[Callable[[], str], str] = ""

# Multiple favicons can be specified here. The "href" property
# is mandatory, but "sizes," "type," and "rel" are optional.
# For example:
# {
#     "href":path/to/image.png",
#     "sizes": "16x16",
#     "type": "image/png"
#     "rel": "icon"
# },
FAVICONS = [{"href": "/static/assets/images/obsd-favicon.png"}]

# The allowed translation for you app
LANGUAGES = {
    "pt_BR": {"flag": "pt_BR", "name": "Português"},
    "en": {"flag": "us", "name": "English"},
    "es": {"flag": "es", "name": "Spanish"},
}

# THEME_OVERRIDES is used for adding custom theme to superset
# example code for "My theme" custom scheme
# THEME_OVERRIDES = {
#   "borderRadius": 4,
#   "colors": {
#       "text": {
#           "base": "red",
#       },
#       "primary": {
#           "base": 'red',
#       },
#       "secondary": {
#           "base": 'green',
#       },
#       "grayscale": {
#           "base": 'orange',
#       }
#   }
# }

FILTER_STATE_CACHE_CONFIG = {
    'CACHE_TYPE': 'RedisCache',
    'CACHE_DEFAULT_TIMEOUT': 86400,
    'CACHE_KEY_PREFIX': 'superset_filter_cache',
    'CACHE_REDIS_URL': f'redis://{REDIS_SERVER}:{REDIS_PORT}/0'
}

EXPLORE_FORM_DATA_CACHE_CONFIG = {
    'CACHE_TYPE': 'RedisCache',
    'CACHE_DEFAULT_TIMEOUT': 86400,
    'CACHE_KEY_PREFIX': 'superset_filter_cache',
    'CACHE_REDIS_URL': f'redis://{REDIS_SERVER}:{REDIS_PORT}/0'
}

CACHE_CONFIG = {
    'CACHE_TYPE': 'RedisCache',
    'CACHE_DEFAULT_TIMEOUT': 86400,
    'CACHE_KEY_PREFIX': 'superset_filter_cache',
    'CACHE_REDIS_URL': f'redis://{REDIS_SERVER}:{REDIS_PORT}/0'
}

DATA_CACHE_CONFIG = {
    'CACHE_TYPE': 'RedisCache',
    'CACHE_DEFAULT_TIMEOUT': 86400,
    'CACHE_KEY_PREFIX': 'superset_filter_cache',
    'CACHE_REDIS_URL': f'redis://{REDIS_SERVER}:{REDIS_PORT}/0'
}

class CeleryConfig(object):
    BROKER_URL = f'redis://{REDIS_SERVER}:{REDIS_PORT}/'
    CELERY_IMPORTS = ('superset.sql_lab', )
    CELERY_RESULT_BACKEND = f'redis://{REDIS_SERVER}:{REDIS_PORT}/'
    CELERY_ANNOTATIONS = {'tasks.add': {'rate_limit': '10/s'}}
    CELERYD_LOG_LEVEL = 'DEBUG'

CELERY_CONFIG = CeleryConfig
HTTP_HEADERS = {
    'super': 'header!'
}
