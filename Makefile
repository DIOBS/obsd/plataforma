build:
	@docker compose build
up:
	@docker compose up
down:
	@docker compose down -v || true
status:
	@docker image ls | grep obsd || true
	@docker volume ls | grep obsd || true

all: down build up
